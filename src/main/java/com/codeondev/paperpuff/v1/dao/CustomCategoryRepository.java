package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.Category;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CustomCategoryRepository {
    List<Category> getAllCategories();
    Category addCategory(Category category);
    Category getCategory(String id);
    Category deleteCategory(String id);
}
