package com.codeondev.paperpuff.v1.service;

import com.codeondev.paperpuff.v1.dao.FeaturedImageRepository;
import com.codeondev.paperpuff.v1.model.FeaturedImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeaturedImagesService {

    @Autowired
    FeaturedImageRepository repository;

    public List<FeaturedImage> getFeaturedImages() {
        return repository.findAll();
    }

    public List<FeaturedImage> addFeaturedImages(List<FeaturedImage> featuredImages) {
        return repository.saveAll(featuredImages);
    }
}
