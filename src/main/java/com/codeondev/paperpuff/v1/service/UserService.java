package com.codeondev.paperpuff.v1.service;

import com.codeondev.paperpuff.v1.model.ConfirmationToken;
import com.codeondev.paperpuff.v1.model.User;
import com.codeondev.paperpuff.v1.model.UserDetailsImpl;
import com.codeondev.paperpuff.v1.model.UserRegister;
import com.codeondev.paperpuff.v1.dao.CustomUserRepository;
import com.codeondev.paperpuff.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    CustomUserRepository repository;

    @Autowired
    ConfirmationTokenService confirmationTokenService;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    PasswordEncoder encoder;

    public User addUser(UserRegister userRegister) {
        User user = new User(
                userRegister.getName(),
                userRegister.getUsername(),
                userRegister.getEmail(),
                encoder.encode(userRegister.getPassword()),
                Arrays.asList("ROLE_USER"));

        user = repository.addUser(user);
        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken =
                new ConfirmationToken(user.get_id(),
                        token,
                        LocalDateTime.now(),
                        LocalDateTime.now().plusMinutes(15));
        confirmationTokenService.saveConfirmationToken(confirmationToken);
        return user;
    }

    public User getUser(String id) {
        return repository.getUser(id);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByUserName(username);
        return UserDetailsImpl.build(user);
    }

    public String loadUserNameByEmail(String email) {
        return repository.findByEmail(email);
    }
}
