package com.codeondev.paperpuff.v1.controller;

import com.codeondev.paperpuff.v1.model.APIResponse;
import com.codeondev.paperpuff.v1.model.Order;
import com.codeondev.paperpuff.v1.service.OrderService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {

    @Autowired
    OrderService orderService;

    @GetMapping("/v1/order/all")
    public APIResponse<List<Order>> getAllOrders() {
        List<Order> orders = orderService.getAllOrders();
        return new APIResponse<>(orders.size(), orders);
    }

    @GetMapping("/v1/order/{id}")
    public Order getOrder(@PathVariable String id) {
        return orderService.getOrder(id);
    }

    @GetMapping("/v1/order/user/{userId}")
    public APIResponse<List<Order>> getOrdersByUserId(@PathVariable String userId) {
        List<Order> orders = orderService.getOrdersByUserId(userId);
        return new APIResponse<>(orders.size(), orders);
    }

    @PostMapping("/v1/order/add")
    public Order addOrder(@RequestBody Order order) {
        return orderService.addOrder(order);
    }

    @PostMapping("/v1/order/update")
    public Order updateOrder(@RequestBody Order order) {
        return orderService.updateOrder(order);
    }

    @GetMapping("/v1/order/{id}/status")
    public String getOrderStatus(@PathVariable String id) {
        return orderService.getOrderStatus(id);
    }

    @PostMapping("/v1/order/{id}/status")
    public Order updateOrderStatus(@PathVariable String id, @RequestBody String s) {
        String status = new JSONObject(s).getString("status");
        return orderService.updateOrderStatus(id, status);
    }

    @DeleteMapping("/v1/order/{id}")
    public Order deleteOrder(@PathVariable String id) {
        return orderService.deleteOrder(id);
    }

    @DeleteMapping("/v1/order/user/{userId}")
    public List<Order> deleteOrderbyUserId(@PathVariable String userId) {
        return orderService.deleteOrdersByUserId(userId);
    }
}
