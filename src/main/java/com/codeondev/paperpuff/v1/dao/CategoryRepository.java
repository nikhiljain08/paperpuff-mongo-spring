package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.Category;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public interface CategoryRepository extends MongoRepository<Category, String>, CustomCategoryRepository {

}
