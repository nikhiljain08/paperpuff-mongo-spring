package com.codeondev.paperpuff.v1.service;

import com.codeondev.paperpuff.v1.dao.CustomOrderRepository;
import com.codeondev.paperpuff.v1.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {

    @Autowired
    private CustomOrderRepository repository;

    public List<Order> getAllOrders() {
        return repository.getAllOrders();
    }
    public Order getOrder(String id) {
        return repository.getOrder(id);
    }
    public Order deleteOrder(String id) {
        return repository.deleteOrder(id);
    }
    public List<Order> getOrdersByUserId(String user_id) {
        return repository.getOrdersByUserId(user_id);
    }
    public List<Order> deleteOrdersByUserId(String user_id) {
        return repository.deleteOrdersByUserId(user_id);
    }
    public Order addOrder(Order order) {
        return repository.addOrder(order);
    }
    public String getOrderStatus(String id) {
        return repository.getOrderStatus(id);
    }
    public Order updateOrderStatus(String id, String status) {
        return repository.updateOrderStatus(id, status);
    }
    public Order updateOrder(Order order) {
        return repository.updateOrder(order);
    }
}
