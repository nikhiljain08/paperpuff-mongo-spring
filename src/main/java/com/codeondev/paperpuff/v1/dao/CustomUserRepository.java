package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.User;

public interface CustomUserRepository {
    User addUser(User User);
    User getUser(String id);
    User findByUserName(String username);
    String findByEmail(String email);
}
