package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.RefreshToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class CustomRefreshTokenRepositoryImpl implements CustomRefreshTokenRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public RefreshToken findByTokenId(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return mongoTemplate.findOne(query, RefreshToken.class);
    }

    @Override
    public RefreshToken findByToken(String token) {
        Query query = new Query();
        query.addCriteria(Criteria.where("token").is(token));
        return mongoTemplate.findOne(query, RefreshToken.class);
    }

    @Override
    public RefreshToken deleteByUsername(String username) {
        Query query = new Query();
        query.addCriteria(Criteria.where("username").is(username));
        return mongoTemplate.findAndRemove(query, RefreshToken.class);
    }
}
