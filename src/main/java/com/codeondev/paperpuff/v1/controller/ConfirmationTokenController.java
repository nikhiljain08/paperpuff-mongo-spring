package com.codeondev.paperpuff.v1.controller;

import com.codeondev.paperpuff.exception.CustomException;
import com.codeondev.paperpuff.v1.model.ConfirmationToken;
import com.codeondev.paperpuff.v1.service.ConfirmationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@RestController
public class ConfirmationTokenController {

    @Autowired
    ConfirmationTokenService confirmationTokenService;

    @GetMapping("/v1/confirm")
    public String confirm(HttpServletRequest request, @RequestParam("token") String token) throws CustomException {
        return confirmationTokenService.confirmToken(request, token);
    }
}
