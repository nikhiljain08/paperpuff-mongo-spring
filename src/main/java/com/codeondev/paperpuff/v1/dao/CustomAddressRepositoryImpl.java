package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

public class CustomAddressRepositoryImpl implements CustomAddressRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Address> getAllAddress() {
        return mongoTemplate.findAll(Address.class);
    }

    @Override
    public Address getAddressById(String id) {
        return mongoTemplate.findById(id, Address.class);
    }

    @Override
    public List<Address> getAddressByUserId(String id) {
        return mongoTemplate.find(new Query().addCriteria(Criteria.where("user_id").is(id)), Address.class);
    }

    @Override
    public Address addAddress(Address address) {
        return mongoTemplate.save(address);
    }

    @Override
    public Address deleteAddress(String id) {
        return mongoTemplate.findAndRemove(new Query().addCriteria(Criteria.where("_id").is(id)), Address.class);
    }
}
