package com.codeondev.paperpuff.v1.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "featuredImages")
public class FeaturedImage {

    @Id
    String id;
    int position;
    String image_url;

    public FeaturedImage() {
    }

    public FeaturedImage(String id, int position, String image_url) {
        this.id = id;
        this.position = position;
        this.image_url = image_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @Override
    public String toString() {
        return "FeaturedImage{" +
                "id='" + id + '\'' +
                ", position=" + position +
                ", image_url='" + image_url + '\'' +
                '}';
    }
}
