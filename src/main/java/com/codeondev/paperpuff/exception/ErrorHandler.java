package com.codeondev.paperpuff.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ErrorHandler {

    @ExceptionHandler({CustomException.class})
    public ResponseEntity<CustomErrorResponse> handleException(CustomException e) {
        CustomErrorResponse errorResponse = new CustomErrorResponse(e.getStatus().value(),
                e.getStatus().getReasonPhrase(),
                e.getLocalizedMessage(),
                e.getRequest().getRequestURI());
        return new ResponseEntity<>(errorResponse, e.getStatus());
    }

}
