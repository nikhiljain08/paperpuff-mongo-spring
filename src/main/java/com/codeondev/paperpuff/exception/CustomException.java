package com.codeondev.paperpuff.exception;

import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;

public class CustomException extends Exception {

    private String message;
    private Throwable cause;
    private HttpStatus status;
    private HttpServletRequest request;

    public CustomException(HttpServletRequest request, HttpStatus status, String message, Throwable cause) {
        this.request = request;
        this.message = message;
        this.status = status;
        this.cause = cause;
    }

    public CustomException(HttpServletRequest request, HttpStatus status, String message) {
        this.request = request;
        this.message = message;
        this.status = status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public Throwable getCause() {
        return cause;
    }

    public void setCause(Throwable cause) {
        this.cause = cause;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
}
