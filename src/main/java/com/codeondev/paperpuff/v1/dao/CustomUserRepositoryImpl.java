package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class CustomUserRepositoryImpl implements CustomUserRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public User addUser(User user) {
        Query query = new Query();
        query.addCriteria(new Criteria().orOperator(
                Criteria.where("username").is(user.getUsername()),
                Criteria.where("email").is(user.getEmail())
        ));
        User alreadyUser = mongoTemplate.findOne(query, User.class);
        return Objects.requireNonNullElseGet(alreadyUser, () -> mongoTemplate.insert(user));
    }

    @Override
    public User findByUserName(String username) {
        return mongoTemplate.findOne(new Query().addCriteria(Criteria.where("username").is(username)), User.class);
    }

    @Override
    public String findByEmail(String email) {
        User user = mongoTemplate.findOne(new Query().addCriteria(Criteria.where("email").is(email)), User.class);
        if(user!=null) {
            return user.getUsername();
        }
        return null;
    }

    @Override
    public User getUser(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        return mongoTemplate.findOne(query, User.class);
    }

}
