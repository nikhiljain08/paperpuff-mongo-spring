package com.codeondev.paperpuff.v1.model;

public class APIResponse<T> {

    int count;
    T response;

    public APIResponse() {
    }

    public APIResponse(int count, T response) {
        this.count = count;
        this.response = response;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }
}
