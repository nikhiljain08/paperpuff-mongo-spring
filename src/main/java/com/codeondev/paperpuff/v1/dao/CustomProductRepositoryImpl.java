package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomProductRepositoryImpl implements CustomProductRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Product> getAllProducts() {
        return mongoTemplate.findAll(Product.class);
    }

    @Override
    public Product addProduct(Product product) {
        return mongoTemplate.insert(product);
    }

    @Override
    public Product getProduct(String id) {
        return mongoTemplate.findById(id, Product.class);
    }

    @Override
    public Product deleteProduct(String id) {
        return mongoTemplate.findAndRemove(new Query().addCriteria(Criteria.where("_id").is(id)), Product.class);
    }

    @Override
    public List<Product> deleteProductByCategoryId(String id) {
        return mongoTemplate.findAllAndRemove(new Query().addCriteria(Criteria.where("category_id").is(id)), Product.class);
    }

    @Override
    public List<Product> getProductsUsingCategory(String id) {
        return mongoTemplate.find(new Query().addCriteria(Criteria.where("category_id").is(id)), Product.class);
    }
}
