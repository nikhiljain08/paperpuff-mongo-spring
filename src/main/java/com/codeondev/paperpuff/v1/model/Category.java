package com.codeondev.paperpuff.v1.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Document(collection = "category")
public class Category implements Serializable {

    @Id
    private String _id;
    private String name;
    private List<Product> products;
    private List<String> tags;

    public Category() {
    }

    public Category(String _id, String name, List<Product> products, List<String> tags) {
        this._id = _id;
        this.name = name;
        this.products = products;
        this.tags = tags;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "Category{" +
                "_id=" + _id +
                ", name='" + name + '\'' +
                ", products=" + products +
                ", tags=" + tags +
                '}';
    }
}
