package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.Address;
import com.codeondev.paperpuff.v1.model.Order;
import com.codeondev.paperpuff.v1.model.Product;
import com.codeondev.paperpuff.v1.service.AddressService;
import com.codeondev.paperpuff.v1.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.ArrayList;
import java.util.List;

public class CustomOrderRepositoryImpl implements CustomOrderRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    ProductService productService;

    @Autowired
    AddressService addressService;

    @Override
    public List<Order> getAllOrders() {
        List<Order> orders = mongoTemplate.findAll(Order.class);
        orders.forEach(order -> {
            List<String> productIdList = order.getProductIdList();
            List<Product> products = new ArrayList<>();
            productIdList.forEach(product -> {
                products.add(productService.getProduct(product));
            });
            order.setProductList(products);

            Address address = addressService.getAddressById(order.getAddress_id());
            order.setAddress(address);
        });

        return orders;
    }

    @Override
    public Order addOrder(Order o) {
        List<String> productIdList;
        List<Product> products = new ArrayList<>();
        Order order = mongoTemplate.save(o);
        productIdList = order.getProductIdList();

        productIdList.forEach(product -> products.add(productService.getProduct(product)));
        order.setProductList(products);

        Address address = addressService.getAddressById(order.getAddress_id());
        order.setAddress(address);
        return order;
    }

    @Override
    public Order getOrder(String id) {
        List<Product> products = new ArrayList<>();

        Order order = mongoTemplate.findById(id, Order.class);
        if(order!=null) {
            List<String> productIdList = new ArrayList<>(order.getProductIdList());

            productIdList.forEach(product -> products.add(productService.getProduct(product)));
            order.setProductList(products);

            Address address = addressService.getAddressById(order.getAddress_id());
            order.setAddress(address);
        }
        return order;
    }

    @Override
    public List<Order> getOrdersByUserId(String user_id) {
        List<Order> orders = mongoTemplate.find(new Query().addCriteria(Criteria.where("user_id").is(user_id)), Order.class);
        orders.forEach(order -> {
            List<String> productList = order.getProductIdList();
            List<Product> products = new ArrayList<>();
            productList.forEach(product -> {
                products.add(productService.getProduct(product));
            });
            order.setProductList(products);

            Address address = addressService.getAddressById(order.getAddress_id());
            order.setAddress(address);
        });
        return orders;
    }

    @Override
    public List<Order> deleteOrdersByUserId(String user_id) {
        List<Order> orders = mongoTemplate.findAllAndRemove(new Query().addCriteria(Criteria.where("user_id").is(user_id)), Order.class);
        orders.forEach(order -> {
            order.setProductList(new ArrayList<>());
        });
        return orders;
    }

    @Override
    public Order deleteOrder(String id) {
        Order order = mongoTemplate.findAndRemove(new Query().addCriteria(Criteria.where("_id").is(id)), Order.class);
        if(order!=null) {
            order.setProductList(new ArrayList<>());
        }
        return order;
    }

    @Override
    public String getOrderStatus(String id) {
        Order order = mongoTemplate.findOne(new Query().addCriteria(Criteria.where("_id").is(id)), Order.class);
        if(order!=null) {
            return order.getStatus();
        }
        return "";
    }

    @Override
    public Order updateOrderStatus(String id, String status) {
        Order order = mongoTemplate.findById(id, Order.class);
        assert order != null;
        order.setStatus(status);
        return mongoTemplate.save(order);
    }

    @Override
    public Order updateOrder(Order order) {
        return mongoTemplate.save(order, "orders");
    }
}
