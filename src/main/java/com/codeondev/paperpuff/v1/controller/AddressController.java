package com.codeondev.paperpuff.v1.controller;

import com.codeondev.paperpuff.v1.model.Address;
import com.codeondev.paperpuff.v1.model.Category;
import com.codeondev.paperpuff.v1.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AddressController {

    @Autowired
    AddressService addressService;

    @GetMapping("/v1/address/all")
    public List<Address> getAllAddress() {
        return addressService.getAllAddress();
    }

    @GetMapping("/v1/address/{id}")
    public Address getAddressbyId(@PathVariable String id) {
        return addressService.getAddressById(id);
    }

    @GetMapping("/v1/address/user/{id}")
    public List<Address> getAddressbyUserId(@PathVariable String id) {
        return addressService.getAddressByUserId(id);
    }

    @PostMapping("/v1/address/add")
    public Address addAddress(@RequestBody Address address) {
        return addressService.addAddress(address);
    }

    @DeleteMapping(path = "/v1/address/{id}")
    private Address deleteAddress(@PathVariable String id) {
        return addressService.deleteAddress(id);
    }
}
