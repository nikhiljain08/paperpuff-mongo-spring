package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.RefreshToken;

public interface CustomRefreshTokenRepository {
    RefreshToken findByTokenId(String id);
    RefreshToken findByToken(String token);
    RefreshToken deleteByUsername(String username);
}
