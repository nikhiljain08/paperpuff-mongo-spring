package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.FeaturedImage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FeaturedImageRepository extends MongoRepository<FeaturedImage, String>  {
}
