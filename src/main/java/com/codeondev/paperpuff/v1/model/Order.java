package com.codeondev.paperpuff.v1.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "orders")
public class Order {

    @Id
    String _id;
    String status;
    double total_amount;
    List<String> productIdList = new ArrayList<>();
    List<Product> productList = new ArrayList<>();
    String user_id;
    String address_id;
    Address address;
    double delivery_amount;
    String payment_mode;
    String payment_id;

    public Order() {
    }

    public Order(String _id, String status, double total_amount, List<String> productIdList, List<Product> productList,
                 String user_id, String address_id, double delivery_amount, String payment_mode, String payment_id) {
        this._id = _id;
        this.status = status;
        this.total_amount = total_amount;
        this.productIdList = productIdList;
        this.productList = productList;
        this.user_id = user_id;
        this.address_id = address_id;
        this.delivery_amount = delivery_amount;
        this.payment_mode = payment_mode;
        this.payment_id = payment_id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(double total_amount) {
        this.total_amount = total_amount;
    }

    public List<String> getProductIdList() {
        return productIdList;
    }

    public void setProductIdList(List<String> productIdList) {
        this.productIdList = productIdList;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public double getDelivery_amount() {
        return delivery_amount;
    }

    public void setDelivery_amount(double delivery_amount) {
        this.delivery_amount = delivery_amount;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    @Override
    public String toString() {
        return "Order{" +
                "_id='" + _id + '\'' +
                ", status='" + status + '\'' +
                ", total_amount=" + total_amount +
                ", productIdList=" + productIdList +
                ", productList=" + productList +
                ", user_id='" + user_id + '\'' +
                ", address_id='" + address_id + '\'' +
                ", address=" + address +
                ", delivery_amount=" + delivery_amount +
                ", payment_mode='" + payment_mode + '\'' +
                ", payment_id='" + payment_id + '\'' +
                '}';
    }
}
