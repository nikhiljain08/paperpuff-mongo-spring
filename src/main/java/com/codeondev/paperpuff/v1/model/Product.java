package com.codeondev.paperpuff.v1.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "products")
public class Product {

    @Id
    private String _id;
    private String category_id;
    private boolean active;
    private double base_price;
    private int base_qty;
    private String description;
    private String img_url;
    private String name;
    private double original_price;

    public Product() {
    }

    public Product(String _id, String category_id, boolean active, double base_price,
                   int base_qty, String description, String img_url,
                   String name, double original_price) {
        this._id = _id;
        this.category_id = category_id;
        this.active = active;
        this.base_price = base_price;
        this.base_qty = base_qty;
        this.description = description;
        this.img_url = img_url;
        this.name = name;
        this.original_price = original_price;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getBase_price() {
        return base_price;
    }

    public void setBase_price(double base_price) {
        this.base_price = base_price;
    }

    public int getBase_qty() {
        return base_qty;
    }

    public void setBase_qty(int base_qty) {
        this.base_qty = base_qty;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(double original_price) {
        this.original_price = original_price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "_id=" + _id +
                ", category_id=" + category_id +
                ", active=" + active +
                ", base_price=" + base_price +
                ", base_qty=" + base_qty +
                ", description='" + description + '\'' +
                ", img_url='" + img_url + '\'' +
                ", name='" + name + '\'' +
                ", original_price=" + original_price +
                '}';
    }
}
