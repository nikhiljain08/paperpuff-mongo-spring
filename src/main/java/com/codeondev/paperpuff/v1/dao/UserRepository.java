package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.User;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public interface UserRepository extends MongoRepository<User,String>, CustomUserRepository {
}
