package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.Product;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CustomProductRepository {
    List<Product> getAllProducts();
    Product addProduct(Product product);
    Product getProduct(String id);
    Product deleteProduct(String id);
    List<Product> deleteProductByCategoryId(String id);
    List<Product> getProductsUsingCategory(String id);
}
