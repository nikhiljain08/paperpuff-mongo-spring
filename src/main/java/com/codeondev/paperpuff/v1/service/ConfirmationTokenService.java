package com.codeondev.paperpuff.v1.service;

import com.codeondev.paperpuff.exception.CustomException;
import com.codeondev.paperpuff.v1.dao.CustomConfirmationTokenRepository;
import com.codeondev.paperpuff.v1.model.ConfirmationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Service
public class ConfirmationTokenService {

    @Autowired
    CustomConfirmationTokenRepository repository;

    public ConfirmationToken findByToken(String token) {
        return repository.findByToken(token);
    }

    public String confirmToken(HttpServletRequest request, String token) throws CustomException {
        return repository.confirmToken(request, token);
    }

    public ConfirmationToken saveConfirmationToken(ConfirmationToken confirmationToken) {
        return repository.saveConfirmationToken(confirmationToken);
    }
}
