package com.codeondev.paperpuff.v1.controller;

import com.codeondev.paperpuff.v1.model.FeaturedImage;
import com.codeondev.paperpuff.v1.service.FeaturedImagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FeatureImageController {

    @Autowired
    FeaturedImagesService imagesService;

    @GetMapping("/v1/featuredimages")
    public List<FeaturedImage> getFeaturedImages() {
        return imagesService.getFeaturedImages();
    }

    @PostMapping("/v1/featuredimage/add")
    public List<FeaturedImage> addFeaturedImages(@RequestBody List<FeaturedImage> featuredImages) {
        return imagesService.addFeaturedImages(featuredImages);
    }
}
