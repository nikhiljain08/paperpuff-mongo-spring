package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.ConfirmationToken;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public interface ConfirmationTokenRepository
        extends MongoRepository<ConfirmationToken, String>, CustomConfirmationTokenRepository {
}
