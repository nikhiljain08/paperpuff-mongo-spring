package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.exception.CustomException;
import com.codeondev.paperpuff.v1.model.ConfirmationToken;
import com.codeondev.paperpuff.v1.model.User;
import com.codeondev.paperpuff.v1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

public class CustomConfirmationTokenRepositoryImpl implements CustomConfirmationTokenRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    UserService userService;

    @Override
    public ConfirmationToken findByToken(String token) {
        return mongoTemplate.findOne(new Query().addCriteria(Criteria.where("token").is(token)), ConfirmationToken.class);
    }

    @Override
    public String confirmToken(HttpServletRequest request, String token) throws CustomException {
        ConfirmationToken confirmationToken
                = mongoTemplate.findOne(new Query().addCriteria(Criteria.where("token").is(token)), ConfirmationToken.class);

        if(confirmationToken!=null) {
            if (confirmationToken.getConfirmedAt() != null) {
                throw new CustomException(request, HttpStatus.INTERNAL_SERVER_ERROR, "Email already confirmed");
            }

            LocalDateTime expiredAt = confirmationToken.getExpiresAt();

            if (expiredAt.isBefore(LocalDateTime.now())) {
                throw new CustomException(request, HttpStatus.INTERNAL_SERVER_ERROR, "Token expired");
            }

            confirmationToken.setConfirmedAt(LocalDateTime.now());
            mongoTemplate.save(confirmationToken);
            User user = userService.getUser(confirmationToken.getUser_id());
            if (user!=null) {
                user.setActive(true);
                user.setValid(true);
                mongoTemplate.save(user);
            }
        }
        return "CONFIRMED";
    }

    @Override
    public ConfirmationToken saveConfirmationToken(ConfirmationToken confirmationToken) {
        return mongoTemplate.save(confirmationToken);
    }
}
