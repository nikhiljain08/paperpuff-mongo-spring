package com.codeondev.paperpuff.v1.service;

import com.codeondev.paperpuff.v1.dao.CustomCategoryRepository;
import com.codeondev.paperpuff.v1.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CustomCategoryRepository repository;

    public List<Category> getAllCategories() {
        return repository.getAllCategories();
    }
    public Category getCategory(String id) {
        return repository.getCategory(id);
    }
    public Category deleteCategory(String id) {
        return repository.deleteCategory(id);
    }
    public Category addCategory(@RequestBody Category category) {
        return repository.addCategory(category);
    }
}
