package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.Order;

import java.util.List;

public interface CustomOrderRepository {
    List<Order> getAllOrders();
    Order addOrder(Order order);
    Order getOrder(String id);
    List<Order> getOrdersByUserId(String user_id);
    List<Order> deleteOrdersByUserId(String user_id);
    Order deleteOrder(String id);
    String getOrderStatus(String id);
    Order updateOrderStatus(String id, String status);
    Order updateOrder(Order order);
}
