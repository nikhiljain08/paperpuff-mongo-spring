package com.codeondev.paperpuff.v1.controller;

import com.codeondev.paperpuff.v1.model.APIResponse;
import com.codeondev.paperpuff.v1.model.Product;
import com.codeondev.paperpuff.v1.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping(path = "/v1/product/all")
    private APIResponse<List<Product>> getAllProducts() {
        List<Product> products = productService.getAllProducts();
        return new APIResponse<>(products.size(), products);
    }

    @GetMapping(path = "/v1/product/{id}")
    private Product getProduct(@PathVariable String id) {
        return productService.getProduct(id);
    }

    @DeleteMapping(path = "/v1/product/{id}")
    private Product deleteProduct(@PathVariable String id) {
        return productService.deleteProduct(id);
    }

    @DeleteMapping(path = "/v1/product/category/{id}")
    private List<Product> deleteProductByCategoryId(@PathVariable String id) {
        return productService.deleteProductByCategoryId(id);
    }

    @PostMapping(path = "/v1/product/add")
    private Product addProduct(@RequestBody Product product) {
        return productService.addProduct(product);
    }
}
