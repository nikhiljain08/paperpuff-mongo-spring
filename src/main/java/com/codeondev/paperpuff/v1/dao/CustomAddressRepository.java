package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.Address;

import java.util.List;

public interface CustomAddressRepository  {

    List<Address> getAllAddress();
    Address getAddressById(String id);
    List<Address> getAddressByUserId(String id);
    Address addAddress(Address address);
    Address deleteAddress(String id);
}
