package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.v1.model.Category;
import com.codeondev.paperpuff.v1.model.Product;
import com.codeondev.paperpuff.v1.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomCategoryRepositoryImpl implements CustomCategoryRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    ProductService productService;

    @Override
    public List<Category> getAllCategories() {
        List<Category> categories = mongoTemplate.findAll(Category.class);
        categories.forEach(c -> {
            List<Product> products = productService.getProductsUsingCategory(c.getId());
            c.setProducts(products!=null ? products : new ArrayList<>());
        });
        return categories;
    }

    @Override
    public Category addCategory(Category category) {
        Category cat = mongoTemplate.insert(category);
        cat.setProducts(new ArrayList<>());
        return cat;
    }

    @Override
    public Category getCategory(String id) {
        Category category = mongoTemplate.findById(id, Category.class);
        if(category!=null)
            category.setProducts(productService.getProductsUsingCategory(category.getId()));
        return category;
    }

    @Override
    public Category deleteCategory(String id) {
        productService.deleteProductByCategoryId(id);
        return mongoTemplate.findAndRemove(new Query().addCriteria(Criteria.where("_id").is(id)), Category.class);
    }
}
