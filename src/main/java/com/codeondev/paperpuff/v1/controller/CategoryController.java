package com.codeondev.paperpuff.v1.controller;

import com.codeondev.paperpuff.v1.model.APIResponse;
import com.codeondev.paperpuff.v1.service.CategoryService;
import com.codeondev.paperpuff.v1.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping(path = "/v1/category/all")
    private APIResponse<List<Category>> getAllCategories() {
        List<Category> categories = categoryService.getAllCategories();
        return new APIResponse<>(categories.size(), categories);
    }

    @GetMapping(path = "/v1/category/{id}")
    private Category getCategory(@PathVariable String id) {
        return categoryService.getCategory(id);
    }

    @DeleteMapping(path = "/v1/category/{id}")
    private Category deleteCategory(@PathVariable String id) {
        return categoryService.deleteCategory(id);
    }

    @PostMapping(path = "/v1/category/add")
    private Category addCategory(@RequestBody Category category) {
        return categoryService.addCategory(category);
    }
}