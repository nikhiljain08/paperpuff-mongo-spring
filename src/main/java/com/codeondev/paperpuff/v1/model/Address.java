package com.codeondev.paperpuff.v1.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
public class Address {

    @Id
    String _id;
    String user_id;
    String full_name;
    String add_1;
    String add_2;
    String near_by;
    String city;
    String state;
    int pincode;
    String mobile;
    boolean default_add;
    LocalDateTime created_on = LocalDateTime.now();
    LocalDateTime updated_on = LocalDateTime.now();


    public Address() {
    }

    public Address(String _id, String user_id, String full_name, String add_1,
                   String add_2, String near_by, String city, String state,
                   int pincode, String mobile, boolean default_add) {
        this._id = _id;
        this.user_id = user_id;
        this.full_name = full_name;
        this.add_1 = add_1;
        this.add_2 = add_2;
        this.near_by = near_by;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
        this.mobile = mobile;
        this.default_add = default_add;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getAdd_1() {
        return add_1;
    }

    public void setAdd_1(String add_1) {
        this.add_1 = add_1;
    }

    public String getAdd_2() {
        return add_2;
    }

    public void setAdd_2(String add_2) {
        this.add_2 = add_2;
    }

    public String getNear_by() {
        return near_by;
    }

    public void setNear_by(String near_by) {
        this.near_by = near_by;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getPincode() {
        return pincode;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public boolean isDefault_add() {
        return default_add;
    }

    public void setDefault_add(boolean default_add) {
        this.default_add = default_add;
    }

    public LocalDateTime getCreated_on() {
        return created_on;
    }

    public void setCreated_on(LocalDateTime created_on) {
        this.created_on = created_on;
    }

    public LocalDateTime getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(LocalDateTime updated_on) {
        this.updated_on = updated_on;
    }

    @Override
    public String toString() {
        return "Address{" +
                "_id='" + _id + '\'' +
                ", user_id='" + user_id + '\'' +
                ", full_name='" + full_name + '\'' +
                ", add_1='" + add_1 + '\'' +
                ", add_2='" + add_2 + '\'' +
                ", near_by='" + near_by + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", pincode=" + pincode +
                ", mobile='" + mobile + '\'' +
                ", default_add=" + default_add +
                ", created_on=" + created_on +
                ", updated_on=" + updated_on +
                '}';
    }
}
