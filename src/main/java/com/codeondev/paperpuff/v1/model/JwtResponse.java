package com.codeondev.paperpuff.v1.model;

import java.util.List;

public class JwtResponse {

    private String _id;
    private String name;
    private String username;
    private String email;
    private List<String> roles;
    private boolean valid;
    private boolean active;
    private String type = "Basic";
    private String token;
    private String refreshToken;

    public JwtResponse(String _id, String name, String username, String email,
                List<String> roles, boolean valid, boolean active, String token, String refreshToken) {
        this._id = _id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.roles = roles;
        this.valid = valid;
        this.active = active;
        this.token = token;
        this.refreshToken = refreshToken;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    @Override
    public String toString() {
        return "JwtResponse{" +
                "_id='" + _id + '\'' +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", roles=" + roles +
                ", valid=" + valid +
                ", active=" + active +
                ", type='" + type + '\'' +
                ", token='" + token + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                '}';
    }
}
