package com.codeondev.paperpuff.v1.dao;

import com.codeondev.paperpuff.exception.CustomException;
import com.codeondev.paperpuff.v1.model.ConfirmationToken;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

public interface CustomConfirmationTokenRepository {
    ConfirmationToken findByToken(String token);
    String confirmToken(HttpServletRequest request, String token) throws CustomException;
    ConfirmationToken saveConfirmationToken(ConfirmationToken confirmationToken);
}
