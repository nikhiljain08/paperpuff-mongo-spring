package com.codeondev.paperpuff.v1.service;

import com.codeondev.paperpuff.v1.dao.CustomAddressRepository;
import com.codeondev.paperpuff.v1.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {

    @Autowired
    CustomAddressRepository customAddressRepository;

    public List<Address> getAllAddress() {
        return customAddressRepository.getAllAddress();
    }

    public Address getAddressById(String id) {
        return customAddressRepository.getAddressById(id);
    }

    public List<Address> getAddressByUserId(String id) {
        return customAddressRepository.getAddressByUserId(id);
    }

    public Address addAddress(Address address) {
        return customAddressRepository.addAddress(address);
    }

    public Address deleteAddress(String id) {
        return customAddressRepository.deleteAddress(id);
    }
}
