package com.codeondev.paperpuff.v1.controller;

import com.codeondev.paperpuff.exception.CustomException;
import com.codeondev.paperpuff.util.EmailValidator;
import com.codeondev.paperpuff.v1.model.*;
import com.codeondev.paperpuff.v1.service.RefreshTokenService;
import com.codeondev.paperpuff.v1.service.UserService;
import com.codeondev.paperpuff.util.JwtTokenUtil;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    RefreshTokenService refreshTokenService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private EmailValidator emailValidator;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    public UserController() {
    }

    @PostMapping(value = "/v1/user/register")
    public User addUser(HttpServletRequest request, @RequestBody UserRegister userRegister)
            throws CustomException {
        User user = userService.addUser(userRegister);
        if(user == null) {
            throw new CustomException(request, HttpStatus.INTERNAL_SERVER_ERROR,
                    "User not registered", new InvalidParameterException());
        }
        return user;
    }

    @GetMapping(value = "/v1/user/{id}")
    public User getUser(HttpServletRequest request, @PathVariable String id)
            throws CustomException {
        System.out.println(request.getRequestURI());
        User user = userService.getUser(id);
        if(user == null) {
            throw new CustomException(request, HttpStatus.NOT_FOUND, "User not found");
        }
        return user;
    }

    @PostMapping(value = "/v1/user/authenticate")
    public ResponseEntity<?> authenticateUser(HttpServletRequest request,
                                              @RequestBody AuthRequest authRequest)
            throws Exception {
        String username = authRequest.getUsername();
        boolean isValidEmail = emailValidator.test(username);
        if (isValidEmail) {
            username = userService.loadUserNameByEmail(username);
            if(username == null || username.isEmpty()) {
                throw new CustomException(request, HttpStatus.NOT_FOUND, "User not found");
            }
        }

        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username,
                            authRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            String jwt = jwtTokenUtil.generateToken(userDetails.getUsername());
            List<String> roles = userDetails.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toList());
            RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getUsername());
            return ResponseEntity.ok(new JwtResponse(userDetails.getId(),
                    userDetails.getName(),
                    userDetails.getUsername(),
                    userDetails.getEmail(),
                    roles,
                    userDetails.isValid(),
                    userDetails.isActive(),
                    jwt, refreshToken.getToken()));
        }  catch (DisabledException e) {
            throw new CustomException(request, HttpStatus.BAD_REQUEST, "User Disabled", e);
        } catch (BadCredentialsException e) {
            throw new CustomException(request, HttpStatus.BAD_REQUEST, "Invalid credentials", e);
        } catch (ExpiredJwtException e){
            throw new CustomException(request, HttpStatus.UNAUTHORIZED,"Expired JWT", e);
        }
    }

    @PostMapping(value = "/v1/refreshtoken")
    public ResponseEntity<?> refreshtoken(HttpServletRequest request,
                                          @RequestBody TokenRefreshRequest tokenRefreshRequest)
            throws CustomException {
        String requestRefreshToken = tokenRefreshRequest.getRefreshToken();
        RefreshToken refreshToken = refreshTokenService.findByToken(requestRefreshToken);
        if(refreshToken!=null && refreshTokenService.verifyExpiration(refreshToken)!=null) {
            String token = jwtTokenUtil.generateToken(refreshToken.getUsername());
            return ResponseEntity.ok(new TokenRefreshResponse(token, requestRefreshToken));
        }
        throw new CustomException(request, HttpStatus.INTERNAL_SERVER_ERROR, "Invalid token");
    }
}
