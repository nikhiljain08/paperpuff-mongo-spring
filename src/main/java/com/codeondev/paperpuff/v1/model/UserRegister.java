package com.codeondev.paperpuff.v1.model;

import java.util.List;

public class UserRegister {

    private String _id;
    private String username;
    private String email;
    private String password;
    private String name;
    private List<String> roles;

    public UserRegister() {

    }

    public UserRegister(String _id, String username, String email, String password, String name,
                        List<String> roles) {
        this._id = _id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.name = name;
        this.roles = roles;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "UserRegister{" +
                "_id='" + _id + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", roles=" + roles +
                '}';
    }
}
