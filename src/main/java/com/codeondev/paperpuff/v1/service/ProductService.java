package com.codeondev.paperpuff.v1.service;

import com.codeondev.paperpuff.v1.dao.CustomProductRepository;
import com.codeondev.paperpuff.v1.model.Product;
import com.codeondev.paperpuff.v1.dao.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private CustomProductRepository repository;

    public List<Product> getAllProducts() {
        return repository.getAllProducts();
    }
    public Product getProduct(String id) {
        return repository.getProduct(id);
    }
    public Product deleteProduct(String id) {
        return repository.deleteProduct(id);
    }
    public List<Product> deleteProductByCategoryId(String id) {
        return repository.deleteProductByCategoryId(id);
    }
    public Product addProduct(Product product) {
        return repository.addProduct(product);
    }
    public List<Product> getProductsUsingCategory(String id) {
        return repository.getProductsUsingCategory(id);
    }
}
