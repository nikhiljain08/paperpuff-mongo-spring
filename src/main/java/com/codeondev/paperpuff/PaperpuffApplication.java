package com.codeondev.paperpuff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableAutoConfiguration
@EnableMongoRepositories
@SpringBootApplication
@ComponentScan("com.codeondev.paperpuff")
public class PaperpuffApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaperpuffApplication.class, args);
	}

}
